#My docker images

Visit branches to view docker files.

## Current images :

- Fedora-ansible: Docker container for ansible based on Fedora with systemd
- Latex: Latex image for building moderncv and more basic latex documents
- Nginx: Nginx image based on official release with Diffie-Hellman key generation
